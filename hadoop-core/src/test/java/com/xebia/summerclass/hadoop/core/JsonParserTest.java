package com.xebia.summerclass.hadoop.core;

import static org.junit.Assert.*;

import org.junit.Test;

public class JsonParserTest {
    
    JsonParser parser = new JsonParser();

    @Test
    public void shouldParseData() {
        String data = "{"
                + "\"array\" : [ 1, { \"name\" : \"Billy\" }, null ],"
                + "\"object\" : { \"id\" : 123, \"names\" : [ \"Bob\", \"Bobby\" ]  }"
                + "}";
        
        assertEquals("Billy", parser.parse(data));
    }

}
