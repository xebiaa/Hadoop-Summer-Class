package com.xebia.summerclass.hadoop.core;

import java.io.IOException;
import java.io.StringReader;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

public class JsonParser {

    private ObjectMapper mapper = new ObjectMapper();

    public String parse(String data) {
        String result = null;

        try {
            JsonNode rootNode = mapper.readValue(new StringReader(data), JsonNode.class);
            result = rootNode.path("array").path(1).path("name").getTextValue();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

}
