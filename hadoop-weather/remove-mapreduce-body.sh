#!/bin/sh

FILES="src/main/java/com/xebia/summerclass/hadoop/weather/mapreduce/temp/TemperaturePerMonthMapper.java
src/main/java/com/xebia/summerclass/hadoop/weather/mapreduce/temp/MaxTemperaturePerMonthReducer.java
src/main/java/com/xebia/summerclass/hadoop/weather/mapreduce/temp/AverageTemperaturePerMonthReducer.java"

for FILE in $FILES
do
    perl -i -pe "BEGIN{undef $/;} s/\/\/ BEGIN SOLUTION.*\/\/ END SOLUTION/\/\/ YOUR SOLUTION/smg" $FILE
done
